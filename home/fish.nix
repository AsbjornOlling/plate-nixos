{ pkgs, colors }:

{
  enable = true;
  plugins = [{
    name = "z";
    src = pkgs.fetchFromGitHub {
      owner = "jethrokuan";
      repo = "z";
      rev = "ddeb28a7b6a1f0ec6dae40c636e5ca4908ad160a";
      sha256 = "0c5i7sdrsp0q3vbziqzdyqn4fmp235ax4mn4zslrswvn8g3fvdyh";
    };
  }];
  shellInit = ''
    # this is WIP
    set -u fish_color_cancel "red"
    set -u fish_color_command "blue"
    set -u fish_color_comment "${colors.color4}"
    set -u fish_color_end "red"
    set -u fish_color_error "${colors.color4}"
    set -u fish_color_escape "green"
    set -u fish_color_normal "${colors.foreground}"
    set -u fish_color_operator "${colors.color8}"
    set -u fish_color_param "${colors.color12}"
    set -u fish_color_quote "${colors.color11}"
    set -u fish_color_redirection "${colors.foreground}"
    set -u fish_color_valid_path "blue"
    set -u fish_greeting ""

    alias feh "${pkgs.feh}/bin/feh --force-aliasing"

    # editor aliases
    alias e "$EDITOR"
    alias vi "$EDITOR"
    alias vim "$EDITOR"
  '';
}
