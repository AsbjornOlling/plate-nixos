{ colors }:

''
 {
   "layer": "bottom",
   "position": "top",
   "height": 30,
   "modules-left": ["sway/workspaces", "sway/mode", "custom/media"],
   "modules-center": ["sway/window"],
   "modules-right": ["custom/behind-by", "pulseaudio", "network", "battery", "clock"],
   // Modules configuration
   "sway/workspaces": {
     "disable-scroll": true,
     "all-outputs": false,
     "format": "{icon}",
     "format-icons": {
       "1": "",
       //"2": "",
       "2": "",
       "3": "󰵅",
       "4": "IV",
       "5": "V",
       "6": "VI",
       "7": "VII",
       "8": "VIII",
       "9": "IX",
       "10": "",
       "11": "XI",
       "12": "XII",
       "13": "XIII",
       "14": "XIV",
       "15": "XV",
       "16": "XVI",
       "17": "XXVII",
       "18": "XVIII",
       "19": "XIX",
       "20": "XX",
       "urgent": ""
       // "focused": "",
       // "default": ""
     }
   },
   "clock": {
     // "timezone": "Europe/Copenhagen",
     "format": "  {:%H:%M}",
     "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
     "format-alt": "{:%Y-%m-%d}"
   },
   "cpu": {
     "format": "  {usage}%",
     "tooltip": true
   },
   "memory": {
     "format": "  {used:.2} GiB"
   },
   "temperature": {
     // "thermal-zone": 2,
     // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
     "critical-threshold": 80,
     // "format-critical": "{temperatureC}°C {icon}",
     "format": "{icon} {temperatureC}°C",
     "format-icons": ["", "", ""]
   },
   "battery": {
     "states": {
       // "good": 95,
       "warning": 30,
       "critical": 15
     },
     "format": "{icon}  {capacity}%",
     "format-charging": "󱐋 {capacity}% ",
     "format-plugged": " {capacity}% ",
     "format-alt": "{time} {icon}",
     // "format-good": "", // An empty format will hide the module
     // "format-full": "",
     "format-icons": ["", "", "", "", ""]
   },
   "network": {
     // "interface": "wlp2*", // (Optional) To force the use of this interface
     "format-wifi": "  {essid} ({signalStrength}%) ",
     "format-ethernet": " {ifname}: {ipaddr}/{cidr}",
     "format-linked": " {ifname} (No IP)",
     "format-disconnected": " Offline",
     "format-alt": "{ifname}: {ipaddr}/{cidr}"
   },
   "pulseaudio": {
     // "scroll-step": 1, // %, can be a float
     "format": "{icon} {volume}% {format_source}",
     "format-muted": " {format_source}",
     "format-source": " {volume}% ",
     "format-source-muted": "",
     "format-icons": {
       "headphone": "",
       "hands-free": "",
       "headset": "",
       "phone": "",
       "portable": "",
       "car": "",
       "default": ["", "", ""]
     },
     "on-click": "",
     "format-bluetooth": "{volume}% {icon} {format_source}",
     "format-bluetooth-muted": " {icon} {format_source}"
   },
   // shows how many commits behind upstream the `nixpkgs` input is
   "custom/behind-by": {
        "format": " {}",
        "exec": "${./behindby.sh}",
        "restart-interval": 600
   }
 }
''
