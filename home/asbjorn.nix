home-manager: { pkgs
, lib
, config
, ...
}:
let 
  cfg = config.asbjorn;
in {
  imports = [
    home-manager.nixosModules.home-manager
  ];
  options = {
    asbjorn.colors = lib.mkOption {
      default = ./colors.nix;
      description = "Color scheme for home-manager dotfiles.";
    };

    asbjorn.extraSwayKeybindings = lib.mkOption {
      default = mod: {};
      description = "Extra keybidings for sway.";
    };
  };

  config = let
    colors = import cfg.colors;
  in {
    # dotfile stuff with home-manager
    home-manager.users.asbjorn = { pkgs, ... }:
    {
      home.stateVersion = "22.05";

      # mopidy - a neat interoperable music server
      services.mopidy = {
        enable = true;
        extensionPackages = [
          pkgs.mopidy-spotify
          pkgs.mopidy-iris
          pkgs.mopidy-mpd
          pkgs.mopidy-scrobbler
          pkgs.mopidy-local
        ];
        settings = with (import ../secrets/secrets.nix); {
          spotify = {
            client_id = spotify_client_id;
            client_secret = spotify_client_secret;
          };
          scrobbler = {
            enabled = true;
            username = lastfm_username;
            password = lastfm_password;
          };
          local = {
            media_dir = "~/Music";
            excluded_file_extensions = [
              ".html"
              ".zip"
              ".jpg"
              ".jpeg"
              ".png"
            ];
          };
        };
      };

      # symlink ~/Downloads/ to /tmp/
      home.file = {
        "Downloads".source = pkgs.runCommand "symlink-tmp" { } "ln -s /tmp/ $out";
      } // (import ../secrets/home-files.nix { pkgs = pkgs; });

      # fish, a friendly shell
      programs.fish = (import ./fish.nix { inherit pkgs colors; });

      # direnv: auto-activate virtual environments
      programs.direnv = { enable = true; };

      # git.
      programs.git = {
        enable = true;
        userName = "AsbjornOlling";
        userEmail = "asbjornolling@gmail.com";
        lfs.enable = true;
      };

      # lockscreen
      programs.swaylock = import ./swaylock.nix { inherit pkgs colors; };

      # sway: i3wm for the future
      wayland.windowManager.sway = import ./sway.nix { inherit pkgs colors cfg; };

      wayland.windowManager.hyprland = import ./hyprland.nix { inherit pkgs colors; };

      xdg.configFile = {
        # waybar (statusbar)
        "waybar/config".text = import ./waybar_config.nix { inherit colors; };
        "waybar/style.css".text = import ./waybar_style.nix { inherit colors; };

        # wofi (launcher)
        "wofi/style.css".text = import ./wofi_style.nix { inherit colors; };

        # pscircle: process tree wallpapers
        "pscircle/run.sh".text = import ./pscircle.nix { inherit pkgs colors; sway = pkgs.sway; };
      };

      # termite
      # a terminal that has great vim binds for selecting text
      # but pretty bad emoji rendering
      # programs.termite = import ./termite.nix { inherit colors; };

      # kitty
      # a pretty good terminal. emojis work as I want them to.
      programs.kitty = import ./kitty.nix { inherit pkgs colors; };

      # neovim. this used to be my daily-driver editor
      # programs.neovim = import ./neovim.nix { inherit pkgs colors; };

      # qutebrowser
      programs.qutebrowser = import ./qutebrowser.nix { inherit pkgs colors; };
    }; # </home-manager>
  };
}
