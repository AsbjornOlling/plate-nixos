#!/usr/bin/env bash
lockfile="/etc/nixos/flake.lock"
owner=$(jq -r '.nodes.nixpkgs.original.owner' < $lockfile)
repo=$(jq -r '.nodes.nixpkgs.original.repo' < $lockfile)
target_ref=$(jq -r '.nodes.nixpkgs.original.ref' < $lockfile)
current_rev=$(jq -r '.nodes.nixpkgs.locked.rev' < $lockfile)

json_result=$(http "https://api.github.com/repos/$owner/$repo/compare/$target_ref...$current_rev")
behind_by=$(echo $json_result | jq .behind_by)

echo $behind_by
