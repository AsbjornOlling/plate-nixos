{
  # base16 colorscheme generated using "flavours"

  # special
  foreground = "#c544a8";
  background = "#3b1e24";

  # black
  color0  = "#3b1e24";
  color8  = "#c544a8";

  # red
  color1  = "#5b4147";
  color9  = "#d04a4d";

  # green
  color2  = "#7b6469";
  color10 = "#e4c5cd";

  # yellow
  color3  = "#9c878c";
  color11 = "#cc895e";

  # blue
  color4  = "#bcabaf";
  color12 = "#e0bdb6";

  # magenta
  color5  = "#dcced1";
  color13 = "#c7807b";

  # cyan
  color6  = "#e1d5d8";
  color14 = "#aa6566";

  # white
  color7  = "#e6dddf";
  color15 = "#a98c8d";
}
