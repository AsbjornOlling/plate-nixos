{
  # base16 colorscheme generated using "flavours"
  background = "#3d1f1c";
  foreground = "#c4a39e";

  color0 = "#3d1f1c";
  color8 = "#c4a39e";

  color1 = "#5d423f";
  color9 = "#757a73";

  color2 = "#7c6663";
  color10 = "#607c8e";

  color3 = "#9c8986";
  color11 = "#8d8f87";

  color4 = "#bcacaa";
  color12 = "#717984";

  color5 = "#dccfcd";
  color13 = "#7d7583";

  color6 = "#e1d6d5";
  color14 = "#83747c";

  color7 = "#e6dddc";
  color15 = "#707b77";
}
