rec {
background = color0;
foreground = color7;
color0  = "#2a2337";
color1  = "#4b4453";
color2  = "#6b666f";
color3  = "#8c888b";
color4  = "#acaaa7";
color5  = "#cccbc2";
color6  = "#d4d3cb";
color7  = "#dbdbd4";
color8  = "#af6448";
color9  = "#a26b4e";
color10 = "#826fa4";
color11 = "#878394";
color12 = "#787787";
color13 = "#6f7b74";
color14 = "#72797c";
color15 = "#88978b";
}
