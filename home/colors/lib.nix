{ lib }:

let
  # copied from nix-math
  multiply = builtins.foldl' builtins.mul 1;
  sum = builtins.foldl' builtins.add 0;
  pow = x: times: multiply (lib.replicate times x);

  hexDigits =
    [ "0" "1" "2" "3" "4" "5" "6" "7" "8" "9" "a" "b" "c" "d" "e" "f" ];

  fromHexDigit = char: lib.lists.findFirstIndex (c: c == char) (-1) hexDigits;

  fromHex = str:
    let
      chars = lib.strings.stringToCharacters str;
      digits = builtins.map fromHexDigit chars;
      len = builtins.stringLength str;
      shifted = lib.lists.imap1 (i: dgt: dgt * (pow 16 (len - i))) digits;
    in sum shifted;

  fromHexColor = str: {
    r = fromHex (builtins.substring 1 2 str);
    g = fromHex (builtins.substring 3 2 str);
    b = fromHex (builtins.substring 5 2 str);
  };

  withAlpha = alpha: color: color // { a = alpha; };

  toCssRgba = let s = builtins.toString;
  in color: "rgba(${s color.r}, ${s color.g}, ${s color.b}, ${s color.a})";
in { inherit fromHexColor withAlpha toCssRgba; }
