rec {
background = color0;
foreground = color7;
color0  = "#361b18";
color1  = "#56403a";
color2  = "#75655c";
color3  = "#958a7e";
color4  = "#b5afa1";
color5  = "#d5d5c3";
color6  = "#dbdbcc";
color7  = "#e1e1d5";
color8  = "#687d6f";
color9  = "#c84f62";
color10 = "#f2f4e6";
color11 = "#bdb7a9";
color12 = "#e3bf8b";
color13 = "#9e9d8e";
color14 = "#a5b6a9";
color15 = "#c8785c";
}
