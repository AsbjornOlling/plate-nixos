{
  # base16 colorscheme generated using "flavours"
  background = "#192834";
  foreground = "#4c817d";

  color0 = "#192834";
  color8 = "#4c817d";

  color1 = "#3c4955";
  color9 = "#926d86";

  color2 = "#5f6b76";
  color10 = "#7d7492";

  color3 = "#838c97";
  color11 = "#6969e8";

  color4 = "#a6aeb8";
  color12 = "#697a90";

  color5 = "#c9d0d9";
  color13 = "#5678b9";

  color6 = "#d1d7de";
  color14 = "#6a9cb8";

  color7 = "#d9dee4";
  color15 = "#547e8d";
}
