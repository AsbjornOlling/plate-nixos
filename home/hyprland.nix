{ colors, pkgs }: {
  enable = true;
  xwayland.enable = true;
  settings = {
    "$mod" = "SUPER";
    general = {
      gaps_in = 10;
      gaps_out = 20;
      border_size = 0;
      "col.active_border" = "rgba(88888888)";
      "col.inactive_border" = "rgba(00000088)";

      #allow_tearing = true;
      resize_on_border = true;
    };

    dwindle = {
      preserve_split = true;
      pseudotile = true;
    };
    decoration = {
      rounding = 5;
      blur = {
        enabled = true;
        brightness = 1.0;
        contrast = 1.0;
        noise = 5.0e-2;

        passes = 2;
        size = 10;
      };

      drop_shadow = true;
      shadow_ignore_window = true;
      shadow_offset = "0 2";
      shadow_range = 20;
      shadow_render_power = 3;
      "col.shadow" = "rgba(00000055)";
    };

    gestures = {
      workspace_swipe = true;
      workspace_swipe_forever = true;
    };

    animations = {
      enabled = true;
      animation = [
        "border, 1, 2, default"
        "fade, 1, 4, default"
        "windows, 1, 3, default, popin 80%"
        "workspaces, 1, 2, default, slide"
      ];
    };

    input = {
      kb_layout = "us";
      kb_options = "caps:super";
      kb_variant = "altgr-intl";
      natural_scroll = true;
      touchpad = {
        disable_while_typing = false;
      };
    };
    misc = { force_default_wallpaper = 0; };
    exec-once = [ "waybar" "${pkgs.swaybg}/bin/swaybg -i /home/asbjorn/.bg" ];
    bindm = [ "ALT,mouse:272,movewindow" "ALT,mouse:273,resizewindow" ];
    blurls = [ "waybar" "wofi" ];
    monitor = [
      "eDP-1, 1920x1080, 0x0, 1" # laptop screen
      "DP-1, 3840x2160, 1920x0, 1" # office screen
      "HDMI-A-2, 3840x2160, 0x0, 1" # vertical office screen
      # "HDMI-A-2, 1920x1080, 1920x0, 1" # bornhack hack
      # "DP-1, 1920x1080, 1920x0, 1" # office screen
      ",preferred,auto,1" # generic rule for plugging in random monitors
    ];
    binde = [
      # volume control
      ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+; pkill -RTMIN+8 waybar"
      ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-; pkill -RTMIN+8 waybar"
      ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle; pkill -RTMIN+8 waybar"

      #brightness buttons
      ", XF86MonBrightnessUp, exec, brightnessctl s $(($(brightnessctl g) * 12 / 10))"
      ", XF86MonBrightnessDown, exec, brightnessctl s $(($(brightnessctl g) * 8 / 10))"
    ];
    bind = [
      "SUPER, RETURN, exec, kitty"
      "SUPER SHIFT, RETURN, exec, emacsclient -c"
      "SUPER, D, exec, pkill wofi || wofi -S run"
      "SUPER,F,fullscreen"
      "SUPER SHIFT, Q, killactive"
      "SUPER SHIFT, E, exit"
      "SUPER, r, resizeactive"
      ''
        SUPER, P, exec, ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp -d)" - | ${pkgs.wl-clipboard}/bin/wl-copy''

      # floating mode
      "SUPER SHIFT,SPACE,togglefloating,active"

      ### MOVING AROUND ###
      "SUPER SHIFT,1,movetoworkspacesilent,1"
      "SUPER SHIFT,2,movetoworkspacesilent,2"
      "SUPER SHIFT,3,movetoworkspacesilent,3"
      "SUPER SHIFT,4,movetoworkspacesilent,4"
      "SUPER SHIFT,5,movetoworkspacesilent,5"
      "SUPER SHIFT,6,movetoworkspacesilent,6"
      "SUPER SHIFT,7,movetoworkspacesilent,7"
      "SUPER SHIFT,8,movetoworkspacesilent,8"
      "SUPER SHIFT,9,movetoworkspacesilent,9"
      "SUPER SHIFT,0,movetoworkspacesilent,10"

      "SUPER,1,workspace,1"
      "SUPER,2,workspace,2"
      "SUPER,3,workspace,3"
      "SUPER,4,workspace,4"
      "SUPER,5,workspace,5"
      "SUPER,6,workspace,6"
      "SUPER,7,workspace,7"
      "SUPER,8,workspace,8"
      "SUPER,9,workspace,9"
      "SUPER,0,workspace,10"

      "SUPER,h,movefocus,l"
      "SUPER,j,movefocus,d"
      "SUPER,k,movefocus,u"
      "SUPER,l,movefocus,r"

      "SUPER SHIFT,h,movewindow,l"
      "SUPER SHIFT,j,movewindow,d"
      "SUPER SHIFT,k,movewindow,u"
      "SUPER SHIFT,l,movewindow,r"

      # special workspace (scratchpad)
      "SUPER SHIFT, grave, movetoworkspace, special"
      "SUPER, grave, togglespecialworkspace, eDP-1"
      "SUPER, s, layoutmsg, togglesplit"
    ];
  };
  extraConfig = ''
    # my custom curve
    # (it does not work)
    # bezier=easeinout, 0.65, 0, 0.35, 1

    # window resize
    bind = SUPER, R, submap, resize

    submap = resize
    binde = , l, resizeactive, 30 0
    binde = , h, resizeactive, -30 0
    binde = , k, resizeactive, 0 -30
    binde = , j, resizeactive, 0 30
    binde = SHIFT, l, resizeactive, 1 0
    binde = SHIFT, h, resizeactive, -1 0
    binde = SHIFT, k, resizeactive, 0 -1
    binde = SHIFT, j, resizeactive, 0 1
    bind = , escape, submap, reset
    bind = SUPER, R, submap, reset
    submap = reset
  '';
}
