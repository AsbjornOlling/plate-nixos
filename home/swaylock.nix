{ colors, pkgs, ... }:

{
  enable = true;
  package = pkgs.swaylock-effects;
  settings = {
    font-size = 30;
    fade-in = 300;
    indicator-radius = 200;
    indicator-thickness = 60;
    line-color = "${colors.background}";
    ring-color = "${colors.background}80";
    inside-clear-color = "00000000";
    ring-clear-color = "${colors.color9}";
    ring-wrong-color = "${colors.color9}";
    inside-color = "00000000"; # invisible
    key-hl-color = "${colors.color9}";
    text-color = "${colors.color9}";
    separator-color = "00000000";
    # line-color = "${pkgs.lib.strings.removePrefix "#" colors.color1}";
  };
}


