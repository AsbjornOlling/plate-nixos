# Help is available in the configuration.nix(5) man page and in the NixOS manual (accessible by running ‘nixos-help’).
{ config, pkgs, inputs, ... }:

{
  imports = [ ./hardware-configuration.nix ];
  nixpkgs.overlays = [
    # Overlay 1: Use `self` and `super` to express
    # the inheritance relationship
    (self: super: {
      qutebrowser = super.qutebrowser.override { enableVulkan = false; python3 = pkgs.python311; };
    })
  ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # prevent systemd-boot it from filling all of /boot
  boot.loader.systemd-boot.configurationLimit = 20;

  # kernel modules needed for audio stuff
  # (https://nixos.wiki/wiki/JACK)
  boot.kernelModules = [
    "snd-seq" # sequencing
    "snd-rawmidi" # midi
  ];

  # Networking
  networking = {
    hostName = "plate";
    networkmanager.enable = true;
    nameservers = [ "1.1.1.1" "1.0.0.1" ];

    # The global useDHCP flag is deprecated, therefore explicitly set to false here.
    # Per-interface useDHCP will be mandatory in the future, so this generated config
    # replicates the default behaviour.
    useDHCP = false;
    interfaces.wlp0s20f3.useDHCP = true;

    # vpn
    wireguard.interfaces.am = import ../secrets/wireguard/am.nix;
    # wireguard.interfaces.vpnbox = import ./private/wireguard/vpnbox.nix;
    # wireguard.interfaces.skelby = import ./private/wireguard/skelby.nix;

    firewall = {
      enable = true;
      allowedTCPPorts = [
        1234 # used for random netcatting and stuff
        1337 # used for random netcatting and stuff
      ];
      allowedUDPPorts = [ 51820 ]; # for wireguard
    };

    extraHosts = ''
      127.0.0.1 plate
      10.10.10.111 obsidebian.wg
    '';
  };

  # this was marked as insecure because of known side-channel attacks on the aes implementation that libolm uses
  # and element-desktop depends on jitsi meet which depends on libolm
  # (even though element-desktop uses vodozemac)
  nixpkgs.config.permittedInsecurePackages = [
    "jitsi-meet-1.0.8043"
  ];

  # faster nix-shell builds
  # broken as of Fri 16 Aug 13:36:36 CEST 2024
  # services.lorri.enable = true;

  # pinetime udev rules
  services.udev.packages = [
    (pkgs.writeTextFile {
      name = "probe-rs udev rules for pinetime";
      text = ''ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE="660", GROUP="users", TAG+="uaccess"'';
      destination = "/etc/udev/rules.d/69-probe-rs.rules";
    })
  ];

  # docker: use btrfs as a storageDriver
  virtualisation.docker.storageDriver = "btrfs";

  # user-specific stuff below here
  # users
  users.users.asbjorn = {
    isNormalUser = true;
    extraGroups = [ "audio" "docker" "wheel" "wireshark" ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?
}
