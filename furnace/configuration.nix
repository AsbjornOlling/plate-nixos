# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

{ lib, pkgs, inputmodule, asbjorn-inputmodule, ... }:

let
  fw-inputmodule = inputmodule.packages.x86_64-linux.default;

  kernelPkgs = pkgs.linuxPackages;
in
{
  imports =
    [ # Include the results of the hardware scan.
      asbjorn-inputmodule.nixosModules.asbjorn-inputmodule
      ./hardware-configuration.nix
    ];

  nixpkgs.config.rocmSupport = true;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.systemd-boot.configurationLimit = 100;
  boot.loader.efi.canTouchEfiVariables = true;

  # cors_ec stuff to let input modules register with framework_tool
  boot.kernelPatches = [
    (lib.mkIf (lib.versionOlder kernelPkgs.kernel.version "6.9")
      {
        name = "cros_ec_lpc";
        patch = (pkgs.fetchpatch {
          url = "https://patchwork.kernel.org/series/840830/mbox/";
          sha256 = "sha256-7jSEAGInFC+a+ozCyD4dFz3Qgh2JrHskwz7UfswizFw=";
        });
      })
  ];
  boot.extraModulePackages = [
    kernelPkgs.framework-laptop-kmod
  ];
  boot.kernelModules = [ "cros_ec" "cros_ec_lpcs" ];

  # just a regular-ass hostname
  networking.hostName = "furnace"; 
  # zfs needs hostId to be set. hostId needs to be a 32-bit hex value
  networking.hostId = "4f00face"; 
  # Easiest to use and most distros use this by default
  networking.networkmanager.enable = true;
  # let stubby do it's thing
  networking.networkmanager.dns = "none";
  networking.nameservers = [ "127.0.0.1" "::1" ];

  networking.firewall = {
      enable = true;
      allowedTCPPorts = [
        1234 # used for random netcatting and stuff
        1337 # used for random netcatting and stuff
      ];
      allowedUDPPorts = [ 51820 ]; # for wireguard
    };
  # vpn stuff
  networking.wireguard = import ../secrets/furnace-wireguard;

  # stubby is encrypted dns proxy that does dns-over-tls
  # this one is set up to use censurfridns
  services.stubby = {
    enable = true;
    settings =
      let
        censurfridns_pinset = [
          # pinsets copied from stubby sample config https://github.com/getdnsapi/stubby/blob/develop/stubby.yml.example
          #######  pin for "deic-ore.anycast.censurfridns.dk RSA"
          { digest = "sha256"; value = "2JjZgBZkfjSjs117vX+AnyKeYzJNM38zwsaxHwStWsg="; }
          #######  pin for "deic-ore.anycast.censurfridns.dk ECDSA"
          { digest = "sha256"; value = "UXs8xWXai9ZXBAjDKYDiYl/jbIYtyV/bY2w3F1FFTDs="; }
          #######  pin for "deic-lgb.anycast.censurfridns.dk RSA"
          { digest = "sha256"; value = "oDxJrI/lG1Jhl1J7LvapMlYwlHMphZUODvCDBm0nof8="; }
          #######  pin for "deic-lgb.anycast.censurfridns.dk ECDSA"
          { digest = "sha256"; value = "iYkCUwXdH7sT8qh26zt+r5dbTySL43wgJtLCTHaSH9M="; }
          #######  pin for "kracon.anycast.censurfridns.dk RSA"
          { digest = "sha256"; value = "Clii3HzZr48onFoog7I0ma5QmMPSpOBpCykXqgA0Wn0="; }
          #######  pin for "kracon.anycast.censurfridns.dk ECDSA"
          { digest = "sha256"; value = "6eW98h0+xxuaGQkgNalEU5e/hbgKyUoydpPMY6xcKyY="; }
          #######  pin for "rgnet-iad.anycast.censurfridns.dk RSA"
          { digest = "sha256"; value = "sp2Low3+oTsQljNzs3gkYgLRYo7o91t3XGka+pwX//4="; }
          #######  pin for "rgnet-iad.anycast.censurfridns.dk ECDSA"
          { digest = "sha256"; value = "/NPc7sIUzKLAQbsvRRhK6Ul3jip6Gi49bxutfrzpsQM="; }
        ];
      in
      pkgs.stubby.passthru.settingsExample // {
        upstream_recursive_servers = [ {
          address_data = "91.239.100.100";
          tls_auth_name = "anycast.censurfridns.dk";
          tls_pubkey_pinset = censurfridns_pinset;
        } {
          address_data = "2001:67c:28a4::0";
          tls_auth_name = "anycast.censurfridns.dk";
          tls_pubkey_pinset = censurfridns_pinset;
        }];
      };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.asbjorn = {
    isNormalUser = true;
    extraGroups = [ "nix" "docker" "wheel" ];
  };

  # rice setup
  asbjorn.colors = ../home/colors/greyscale.nix;
  asbjorn.extraSwayKeybindings = let
    setbrightness = pkgs.writeShellScriptBin "setbrightness" ''
      # set brightness
      ${pkgs.brightnessctl}/bin/brightnessctl set $1
      # show it on the led matrixes
      ${fw-inputmodule}/bin/inputmodule-control led-matrix --percentage $(($(${pkgs.brightnessctl}/bin/brightnessctl get)*100/255))
    '';
    ledmatrix = "${asbjorn-inputmodule.packages.x86_64-linux.default}/bin/asbjorn-inputmodule";
  in
  mod: {
    "${mod}+b" = "exec ${setbrightness}/bin/setbrightness +1%";
    "${mod}+Shift+b" = "exec ${setbrightness}/bin/setbrightness 1%-";
    "${mod}+ctrl+b" = "exec ${ledmatrix} --brighter";
    "${mod}+ctrl+Shift+b" = "exec ${ledmatrix} --dimmer";
  };

  environment.systemPackages = with pkgs; [
    framework-tool       # random frameworks-laptop specific cli tools
    fw-inputmodule       # provides inputmodule-ctl, to use LED matrix input modules
    nvtopPackages.amd    # provides `nvtop`, for monitoring gpus
    via                  # for changing qmk config on the keyboard
  ];
  services.udev.packages = [ pkgs.via inputmodule ];

  # udev rule for led matrix
  # from: https://github.com/FrameworkComputer/inputmodule-rs/blob/main/release/50-framework-inputmodule.rules
  services.udev.extraRules = ''
    # Framework Laptop 16 - LED Matrix
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="32ac", ATTRS{idProduct}=="0020", MODE="0666", TAG+="uaccess"
  '';

  # fingerprint sensor
  services.fprintd.enable = true;

  # ollama on my AMD gpu
  # https://wiki.nixos.org/wiki/Ollama
  # broken on Mon 27 Jan 11:12:05 CET 2025
  # issue: https://github.com/NixOS/nixpkgs/issues/376930
  services.ollama = {
    enable = true;
    package = pkgs.ollama-rocm;
    rocmOverrideGfx = "11.0.2";
  };

  services.asbjorn-inputmodule.enable = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.11"; # Did you read the comment?
}

