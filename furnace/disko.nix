disko: { ... }: {
  imports = [
    disko.nixosModules.disko
  ];
  disko.devices = {
    disk = {
      disk0 = {
        device = "/dev/disk/by-id/nvme-Corsair_MP600_CORE_MINI_A7SEB33800J5ZS";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            ESP = {
              type = "EF00";
              size = "500M";
              content = {
                type = "filesystem";
                format = "vfat";
                mountpoint = "/boot";
              };
            };
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
      disk1 = {
        device = "/dev/disk/by-id/vme-eui.ace42e003151ee7b2ee4ac0000000001";
        type = "disk";
        content = {
          type = "gpt";
          partitions = {
            zfs = {
              size = "100%";
              content = {
                type = "zfs";
                pool = "zroot";
              };
            };
          };
        };
      };
    };
    zpool = {
      zroot = {
        type = "zpool";
        mode = {
          # be explicit about vdevs since I don't know how zfs defaults
          # I think if each disk is in its own vdev, zfs will stripe across them
          # /shrug hopefully
          topology = {
            type = "topology";
            vdev = [
              { members = [ "disk0" ]; }
              { members = [ "disk1" ]; }
            ];
            cache = [];
          };
        };
        rootFsOptions = {
          compression = "zstd";
          encryption = "on";
          keyformat = "passphrase";
          keylocation = "prompt";
        };
        mountpoint = "/";
        datasets = {
          nix = {
            type = "zfs_fs";
            mountpoint = "/nix";
          };
          home = {
            type = "zfs_fs";
            mountpoint = "/home";
          };
        };
      };
    };
  };
}
