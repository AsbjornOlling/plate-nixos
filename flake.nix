{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    # nixpkgs.url = "github:NixOS/nixpkgs/master"; # temporary for ollama-rocm 5.5

    # manage dotfiles w/ nix
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # for declarative disk formatting
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";

    # for framework laptop input modules
    inputmodule.url = "github:caffineehacker/nix?dir=/flakes/inputmodule-rs";
    inputmodule.inputs.nixpkgs.follows = "nixpkgs";

    # my homemade LED input module controller
    asbjorn-inputmodule.url = "gitlab:AsbjornOlling/asbjorn-inputmodule";
    # asbjorn-inputmodule.url = "/home/asbjorn/Development/asbjorn-inputmodule";
    asbjorn-inputmodule.inputs.nixpkgs.follows = "nixpkgs";

    # p6s neovim setup
    p6svi.url = "gitlab:p6s/vi";
    p6svi.inputs.nixpkgs.follows = "nixpkgs";
  };
  outputs = { nixpkgs, home-manager, disko, p6svi, ... }@inputs: {
    nixosConfigurations = {
      # my Framework 16 (my soon-to-be daily driver)
      furnace = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          # disk partitioning
          (import ./furnace/disko.nix disko) 

          # config unique for this hardware
          ./furnace/configuration.nix        

          # nixos stuff I want on all machines
          ./common.nix

          # my home-manager setup
          (import ./home/asbjorn.nix home-manager)

          # my editor setup
          p6svi.nixosModules.default
        ];
      };

      # my System76 Lemur Pro
      plate = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = inputs;
        modules = [
          { _module.args = { inherit inputs; }; }
          ./plate/configuration.nix
          ./common.nix
          (import ./home/asbjorn.nix home-manager)

          # my editor setup
          p6svi.nixosModules.default
        ];
      };

      # my thesseus' ship desktop computer
      bedrock = nixpkgs.lib.nixosSystem {
        specialArgs = inputs;
        system = "x86_64-linux";
        modules = [
          ./bedrock/configuration.nix
          ./common.nix
          (import ./home/asbjorn.nix home-manager)
          p6svi.nixosModules.default
        ];
      };
    };
  };
}
