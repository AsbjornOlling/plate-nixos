{ config, lib, pkgs, inputs, ... }:

{
  # enable nix search and other bullshit
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  # internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
  time.timeZone = "Europe/Copenhagen";

  # docker
  virtualisation.docker.enable = true;

  # let inotify do a lot
  boot.kernel.sysctl = {
    # Note that inotify watches consume 1kB on 64-bit machines.
    "fs.inotify.max_user_watches" = 1048576; # default:  8192
    "fs.inotify.max_user_instances" = 1024; # default:   128
    "fs.inotify.max_queued_events" = 32768; # default: 16384
  };

  # fonts
  fonts.packages = with pkgs; [
    # emojione           # an emoji font
    # joypixels          # updated emojione?
    # noto-fonts-emoji
    aileron # YEAH HELVETICA
    # font-awesome # some icons
    inconsolata # decent font
    terminus_font     # better font (that I can't get to work in sway)
    # terminus_font_ttf # better font (that I can't get to work in sway)
    eb-garamond
    nerd-fonts.noto
    nerd-fonts.terminess-ttf
    # font-awesome # this is needed for my waybar icons, but breaks p6s/vi icons...
    # noto-fonts # does this fix neovim icons??
    # jetbrains-mono # does this fix neovim icons??
  ];

  # run unpatched dynamic executables on nixos
  programs.nix-ld.enable = true;

  # sway window manager
  programs.sway = {
    enable = true;
    extraPackages = with pkgs; [ kanshi swayidle waybar xwayland ];
  };

  environment.variables = {
    # needed by qutebrowser
    QT_QPA_PLATFORM = "wayland";
  } // (import ./secrets/secrets.nix).environment;

  # bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # flatpak (gui containers)
  services.flatpak.enable = true;
  xdg.portal.enable = true;
  # maybe this makes via work?
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  # default browser
  xdg.mime.defaultApplications = {
    "text/html" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/http" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/https" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/about" = "org.qutebrowser.qutebrowser.desktop";
    "x-scheme-handler/unknown" = "org.qutebrowser.qutebrowser.desktop";
  };

  # pipewire stuff
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };

  services.syncthing = {
    enable = true;
    user = "asbjorn";
    dataDir = "/home/asbjorn/Sync"; # default data dir - not actually used
    configDir = "/home/asbjorn/.config/syncthing";
  };

  # printing
  services.printing.enable = true;

  # this helps against pip-compile running out of space on /run/user/${UID}
  # by allowing systemd logind to allocate up to 50% of memory to the user runtime dir
  # /shrug idk what this actually is
  services.logind.extraConfig = ''
    RuntimeDirectorySize=50%
  '';

  # gnome keyring (needed for nheko)
  services.gnome.gnome-keyring.enable = true;

  # fish shell
  programs.fish.enable = true;
  users.users.asbjorn = {
    shell = pkgs.fish;
  };

  # such secure
  programs.gnupg.agent.enable = true;

  # fix wireshark permissions
  programs.wireshark.enable = true;

  # yep
  programs.steam.enable = true;
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "aseprite"
    "beeper"
    "discord"
    "steam"
    "steam-original"
    "steam-run"
    "steam-unwrapped"
  ];

  environment.systemPackages = with pkgs; [
    android-tools    # adb and friends
    ansible          # 🐮 cattle management
    ascii            # ascii tables
    asciinema        # terminal recording
    apacheHttpd      # useful for `htpasswd` command
    aseprite         # non-free pixel art editor
    atop             # detailed resource monitor
    audacity         # audio schmaudio
    bandwhich        # network monitoring
    beeper           # non-free proprietary client for hosted open-source matrix bridges
    bintools-unwrapped # strings
    # binwalk        # take binary mess part # broken on Tue 11 Feb 11:05:44 CET 2025
    brightnessctl    # control display brightness
    brave            # yet another browser ¯\_(ツ)_/¯
    # brogue           # a good roguelike, bro. broken on Fri 27 Dec 18:45:05 CET 2024
    btop             # better top
    ungoogled-chromium # another browser
    cloc             # count lines of code
    comma            # less verbose nix shells
    cowsay           # moo!
    direnv           # cwd-based virtual environments
    discord          # some chat app [SIGH]
    dogdns           # 🐕 better dig
    docker           # 🐋 containers
    docker-compose   # 🐳 more containers
    element-desktop  # matrix client
    emocli           # cli emoji picker
    exiftool         # read image metadata
    fd               # search file names
    ffmpeg           # media tool that can do anything
    feh              # simple image viewer
    file             # file type detection util
    firefox-wayland  # another browser
    fx               # a json viewer
    gcc              # C compiler
    git              # version control
    git-crypt        # handle encrypted files transparently in git, useful for secrets in flake
    git-lfs          # for thicc files
    gimp             # 🫑 green is my pepper
    gnumake          # inferior build system
    godot_4          # a nice Free as in speech game engine
    # gomuks           # tui matrix client marked as insecure Tue 20 Aug 11:32:24 CEST 2024 b/c of libolm
    gotop            # like htop but prettier
    hexyl            # xxd but better
    httpie           # better than curl
    ifmetric         # easily set priority for ip routes
    inetutils        # telnet
    jq               # json utility
    keepassxc        # password manager
    kitty            # a terminal with proper emoji rendering
    libreoffice      # a less bad office suite
    mopidy           # a good music program
    mpv              # media playback
    mupdf            # pdf viewer
    ncdu             # ncurses disk usage
    ncmpcpp          # mpd client
    neofetch         # pretty system specs
    # nheko            # a neat-o matrix client 🐱 marked insecure Tue 20 Aug 11:33:07 CEST 2024 b/c of libolm bs
    nix-index        # provides nix-locate, which lets you search for files in nixpkgs
    nix-output-monitor # "nom build" is prettier "nix build"
    nicotine-plus    # soulseek client
    nmap             # network scanning
    nodejs           # ☕📜
    p7zip            # archival tool
    pavucontrol      # gui audio stuff
    pandoc           # document conversions
    pciutils         # lspci
    pipewire         # next gen multipedia routing
    prettyping       # ping with colors
    prismlauncher    # a minecraft launcher
    python312Packages.mypy           # python type checking  - mypy doesn't work with 3.13 yet
    python312Packages.black          # python code formatter - mypy doesn't work with 3.13 yet
    python312Packages.requests       # HTTP for Humans™ - doesn't work with 3.13 s omewhy
    python312Packages.types-requests
    python312Packages.ipython        # ergonomic python repl
    python312Packages.numpy          # fancy arrays
    python312Packages.pwntools       # hacky hacky
    python313        # 🐍 a good lang
    python313Packages.hyrule         # an essential library for hy
    python313Packages.pip            # python package manager
    python313Packages.pip-tools      # pin pip dependencies
    python313Packages.jmespath       # needed by some ansible roles that I use
    qbittorrent      # a good bittorrent client
    qt5.qtwayland    # wayland plugin for QT
    ranger           # tui file manager
    remmina          # an RDP client that Just Works™
    ripgrep          # search file content
    screen           # useful for connecting over serial
    sl               # choo choo!
    solo2-cli        # cli for the solokey v2
    steam            # 🎮 vidya
    superTuxKart     # 🐧🏎
    sway             # window manager
    sway-contrib.grimshot # better screenshots in wayland
    # texlive.combined.scheme-full # LaTeX < does not work with fancy cpu extensions, gsl fails some unit tests b/c of float errors
    thunderbird-bin  # the least bad email client? maybe?
    tmux             # terminal multiplexing
    toilet           # ascii art text
    tor-browser-bundle-bin # 🧅🌐
    tree             # pretty directory view
    units            # unit conversions
    # unityhub         # oh god please no help me - broken on Tue 16 Jan 13:09:52 CET 2024
    usbutils         # lsusb
    viewnior         # a different image viewer
    viu              # terminal feh
    websocat         # socat but websockets
    wdisplays        # gui display management for wayland
    wf-recorder      # easy-to-use wayland screen recorder
    wireguard-tools  # a good vpn
    wireshark        # packet captures
    wl-color-picker  # a color picker, duh
    wl-clipboard     # wayland clipboard cli
    yt-dlp           # youtube-dl but it actually works
    zathura          # a powerful pdf viewer
    zed-editor       # the new thing?
    xdg-desktop-portal # something something screensharing
    xdg-desktop-portal-gtk # something something file picker for via
    xdg-utils        # some desktop programs need xdg_open
    # this roc commit is just a random one i chose
    # because I'm sick of rebuilding all the time
    # (builtins.getFlake "github:roc-lang/roc/b1f7316").packages.x86_64-linux.default
    # (builtins.getFlake "github:roc-lang/roc/b1f7316").packages.x86_64-linux.lang-server
    # make cstimer pretend that it's a real boy
    (pkgs.writeShellScriptBin "cstimer" "${brave}/bin/brave https://cstimer.net --kiosk --new-window")
    (pkgs.writeShellScriptBin "upgrade" ''
      nix flake update --flake /etc/nixos
      nixos-rebuild switch --flake "/etc/nixos#$(cat /etc/hostname)" --log-format internal-json |& ${nix-output-monitor}/bin/nom --json
    '')
  ];

}
